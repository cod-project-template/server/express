module.exports = {
  'env': {
    'commonjs': true,
    'es2021': true,
    'node': true,
    'jest/globals': true, // 單元測試
  },
  'extends': [
    'eslint:recommended',
    'google',
  ],
  'parserOptions': {
    'ecmaVersion': 12,
  },
  'parser': 'babel-eslint',
  'plugins': [
    'jest',
  ],
  'rules': {
    'space-before-function-paren': ['error', {
      'anonymous': 'always',
      'named': 'never',
      'asyncArrow': 'always',
    }],

    'object-curly-spacing': ['error', 'always'],

    'max-len': ['error', {
      'ignoreStrings': true,
      'ignoreTemplateLiterals': true,
      'ignoreUrls': true,
      'ignoreRegExpLiterals': true,
      'ignoreComments': true,
      'ignoreTrailingComments': true,
    }],
  },
};
