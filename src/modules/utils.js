const self = {
  /** 判斷環境是否為 GCP
   * @return {Boolean}
   */
  isGcp() {
    return !!process.env.GAE_APPLICATION;
  },


  /** 包裝 promise，使用 Golang 風格返回錯誤
   * @param {Promise} promise
   * @return {Array}
   */
  to(promise) {
    return promise.then((data) => [null, data]).catch((err) => [err]);
  },
};

module.exports = self;
// export default self;

