const express = require('express');

/** @type {express.Router} */
const router = new express.Router();

// Home page route.
router.get('/', (req, res) => {
  res.status(200).json({
    status: 'suc', msg: 'Hi',
  });
});


module.exports = router;
