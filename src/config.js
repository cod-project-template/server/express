module.exports = {
  timezone: 'Asia/Taipei',
  port: 8080, // express port
  auth: {
    signKey: 'gae server',
    timeout: 8 * 60 * 60 * 1000, // ms
  },
};
