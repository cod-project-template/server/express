const jwt = require('jsonwebtoken');
const config = require('@/config');

const self = {
  /** 生成 JWT Token
   * @param {object} data - 待加密的資料
   * @param {Number | String} expiresIn - 多久後過期。（輸入毫秒或用 zeit/ms 格式表示時間）
   * @return {string} JWT Token
   */
  jwtSign(data, expiresIn = null) {
    return jwt.sign(data, config.auth.signKey, {
      expiresIn: expiresIn || config.auth.timeout,
    });
  },

  /** 驗證 JWT 簽章
   * @param {string} token
   */
  async jwtVerify(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.auth.signKey, (err, data) => {
        if (err) {
          return reject(err);
        }

        resolve(data);
      });
    });
  },
};

module.exports = self;

