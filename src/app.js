'use strict';

const express = require('express');
const app = express();
const helmet = require('helmet');

// 自定義模組
// const utils = require('./modules/utils');
// const { to } = utils;

// const config = require('../config');


// 將 Body Data 轉為 JSON
app.use(express.json());

// 引用 Helmet 安全設定
app.use(helmet());

// --- controllers ---

const v1Controller = require('./controllers/v1/v1.controller');
app.use('/api/v1', v1Controller);


/** 404 查無資源
 * 使用官方建議之 app.use 方法，在 GAE 運行時，
 * 會讓 dispatch.json 部分路由匹配效果不好，故改為使用 app.all
 */
app.all('*', (req, res) => {
  res.status(404).json({
    status: 'err', msg: '查無資源',
  });
}); // */

module.exports = app;
// export default app;
