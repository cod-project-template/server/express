
/* import app from './app';
import utils from './modules/utils'; // */

const app = require('./app');
const utils = require('./modules/utils');


// 自定義模組
const config = require('./config');

app.listen(config.port, () => {
  if (utils.isGcp()) {
    console.log(`GCP server ${config.port} 埠啟動`);
    return;
  }

  console.log(`本地 server ${config.port} 埠啟動`);
});

